#include <iostream>
#include <vector>

using Piramide = std::vector<std::vector<int>>;

int shortestPath(const Piramide& piramide){
	int toReturn = piramide.at(0).at(0);
	size_t indexOfSmallerOfPreviousLine = 0;
	for(size_t i = 1; i < piramide.size(); ++i){ // itera em cada nível da pirâmide

		const auto& left = piramide.at(i).at(indexOfSmallerOfPreviousLine);
		const auto& right = piramide.at(i).at(indexOfSmallerOfPreviousLine+1);

		if(left < right) {
			toReturn += left;
		} else {
			toReturn += right;
			indexOfSmallerOfPreviousLine++;
		}
	}
	return toReturn;
}

int main(){
	Piramide p = {
		   {4},
		  {3,4},
		 {6,5,7},
		{4,1,8,3}
	};
	Piramide p2 = {
		    {0},
		   {0,9},
		  {9,0,9},
		 {9,9,0,9},
		{9,9,0,9,9}
	};
	std::cout << shortestPath(p) << std::endl;
	std::cout << shortestPath(p2) << std::endl;
	return 0;
}
