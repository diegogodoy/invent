﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Mvc;

namespace Controllers
{
    public class ClienteController : ODataController
    {
        private ClienteContext _db;

        //initializing db
        public ClienteController(ClienteContext context)
        {
            _db = context;
            if (context.clientes.Count() == 0)
            {
                context.clientes.Add(
                new Cliente
                    {
                        Nome = "Empresa ltda",
                        Endereco = "Rua Qualquer"
                    }
                );
                context.clientes.Add(
                new Cliente
                    {
                        Nome = "Outra Empresa SA",
                        Endereco = "Avenida T#"
                    }
                );
                context.clientes.Add(
                new Cliente
                    {
                        Nome = "Mercadinho da Esquina",
                        Endereco = "Esquina"
                    }
                );

                /*
                foreach (var b in DataSource.getClientes())
                {
                    context.clientes.Add(b);
                }
                */
                context.SaveChanges();
            }
        }

        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.clientes);
        }

        [EnableQuery]
        public IActionResult Get([FromODataUri]int key)
        {
            return Ok(_db.clientes.FirstOrDefault(c => c.Id == key));
        }

        [EnableQuery]
        public IActionResult GetNome([FromODataUri]int key)
        {
            return Ok(_db.clientes.FirstOrDefault(c => c.Id == key).Nome);
        }

        [EnableQuery]
        public IActionResult GetEndereco([FromODataUri]int key)
        {
            return Ok(_db.clientes.FirstOrDefault(c => c.Id == key).Endereco);
        }

        [EnableQuery]
        public IActionResult GetId([FromODataUri]int key)
        {
            return Ok(_db.clientes.FirstOrDefault(c => c.Id == key).Id);
        }

        [EnableQuery]
        public IActionResult Post([FromODataUri]Cliente cliente)
        {
            _db.clientes.Add(cliente);
            _db.SaveChanges();
            return Created(cliente);
        }

        [EnableQuery]
        public IActionResult DeleteCliente([FromODataUri]int key)
        {
            Cliente b = _db.clientes.FirstOrDefault(c => c.Id == key);
            if (b == null)
            {
                return this.NotFound();
            }

            _db.clientes.Remove(b);
            _db.SaveChanges();
            return Ok();
        }
    }
}
