﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public static class DataSource
    {
        private static IList<Cliente> _clientes { get; set; }

        public static IList<Cliente> getClientes()
        {
            if (_clientes != null)
            {
                return _clientes;
            }

            _clientes = new List<Cliente>();

            // cliente #1
            Cliente cliente1 = new Cliente
            {
                Id = 0,
                Nome = "Empresa ltda",

            };
            _clientes.Add(cliente1);
        
            Cliente cliente2 = new Cliente
            {
                Id = 1,
                Nome = "Outra Empresa SA",
                Endereco = "Avenida T#"
            };

            _clientes.Add(cliente2);
            return _clientes;
        }
    }
}
