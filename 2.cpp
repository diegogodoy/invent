#include <iostream>
#include <stack>
#include <string>
using namespace std::literals;

template<class T>
bool isOpeningPair(const T& t){
	return ((t == '(') || ( t == '[') || (t == '{'));
}
template<class T>
bool isClosingPair(const T& t){
	return ((t == ')') || ( t == ']') || (t == '}'));
}
template<class T>
bool isMatchingPair(const T& opening, const T& closing){
	return (
		((opening == '(') && (closing == ')'))||
		((opening == '[') && (closing == ']'))||
		((opening == '{') && (closing == '}'))
		);
}

template<class T>
T getClosing(const T& opening){
	if(opening == '(') return ')';
	else if( opening == '[') return ']';
	else if( opening == '{')  return '}';
	else return ' '; // essa linha só existe para que o compilador pare de reclamar
}


void parser(const std::string& input){
	std::stack<char> charStack;
	for(auto& c: input){
		if(isOpeningPair(c)){
			charStack.push(c);
		}else{
			if(isClosingPair(c)){
				if(charStack.empty()){
					std::cout << "invalid input, closing unopened pair" << std::endl;
					return;
				} else {
					if(isMatchingPair(charStack.top(), c)){
						charStack.pop();
					} else {
						std::cout << "invalid input, pair doesnt match\n";
						std::cout << "esperado: "<< getClosing(charStack.top()) << " recebido:" << c << std::endl;
						return;
					}
				}
			} else {
					std::cout << "invalid input, not procedence operator" << std::endl;
					return;
			}
		}

	}
	std::cout << ((charStack.empty())?"input ok":"string ended too soon") << std::endl;
}

int main(){
	parser("[}"s);
	return 0;
}
