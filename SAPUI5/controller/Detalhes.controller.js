sap.ui.define(["sap/ui/core/mvc/Controller"], function (Controller) {
	"use strict";
	return Controller.extend("cadastrodeclientes.CadastroDeClientes.controller.Detalhes", {
		_onRouteFound: function (e) {
			var argument = e.getParameter("arguments");
			var view = this.getView();
			var path2 = "/cliente(" + argument.SelectedItem + ")";
			view.bindElement(path2);
			//jQuery.sap.require("sap.m.MessageBox");
			//sap.m.MessageBox.show( //"Você selecionou item(2): " + argument.SelectedItem, {
			//	path2, {
			//		title: "",
			//		actions: [sap.m.MessageBox.Action.OK]
			//	});
		},
		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf cadastrodeclientes.CadastroDeClientes.view.Detalhes
		 */
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("RouteDetalhes").attachMatched(this._onRouteFound, this);
		},
		backButton: function () {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("RouteClientes");
			}
			/**
			 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
			 * (NOT before the first rendering! onInit() is used for that one!).
			 * @memberOf cadastrodeclientes.CadastroDeClientes.view.Detalhes
			 */
			//	onBeforeRendering: function() {
			//
			//	},
			/**
			 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
			 * This hook is the same one that SAPUI5 controls get after being rendered.
			 * @memberOf cadastrodeclientes.CadastroDeClientes.view.Detalhes
			 */
			//	onAfterRendering: function() {
			//
			//	},
			/**
			 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
			 * @memberOf cadastrodeclientes.CadastroDeClientes.view.Detalhes
			 */
			//	onExit: function() {
			//
			//	}
			/**
			 *@memberOf cadastrodeclientes.CadastroDeClientes.controller.Detalhes
			 */
			,
		/**
		 *@memberOf cadastrodeclientes.CadastroDeClientes.controller.Detalhes
		 */
		deleteCliente: function (oEvent) {
			//var model = new sap.ui.model.odata.v4.ODataModel("/WebApi");
			//var model2 = this.getView().getModel();
			var oContext = oEvent.getSource().getBindingContext();
			var Id = oContext.getProperty("Id");
			//até aqui tá ok, Id correto foi obtido

			var delUrl = "/cliente(" + Id + ")";

			jQuery.sap.require("sap.m.MessageBox");
			sap.m.MessageBox.show( //"Você selecionou item(2): " + argument.SelectedItem, {
				"Delete Called: " + delUrl, {
					title: "",
					actions: [sap.m.MessageBox.Action.OK]
				});

			//var m = sap.ui.getCore().getModel("");

			oContext.getModel().remove(delUrl, {
				success: function (oData, oResponse) {
					jQuery.sap.require("sap.m.MessageBox");
					sap.m.MessageBox.show( //"Você selecionou item(2): " + argument.SelectedItem, {
						"Sucess", {
							title: "",
							actions: [sap.m.MessageBox.Action.OK]
						});
					
					var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
					oRouter.navTo("RouteClientes");

				},
				error: function (err, oResponse) {
					jQuery.sap.require("sap.m.MessageBox");
					sap.m.MessageBox.show( //"Você selecionou item(2): " + argument.SelectedItem, {
						"Erro ao remover: " + err.response.statusText, {
							title: "",
							actions: [sap.m.MessageBox.Action.OK]
						});
				}
			});
		}
	});
});