/*global QUnit*/

sap.ui.define([
	"cadastrodeclientes/CadastroDeClientes/controller/Clientes.controller"
], function (oController) {
	"use strict";

	QUnit.module("Clientes Controller");

	QUnit.test("I should test the Clientes controller", function (assert) {
		var oAppController = new oController();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});