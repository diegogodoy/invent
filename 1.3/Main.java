import java.util.regex.*;

public class Main {
	public static int safeStringToInteger(String s) throws Exception{
		if(s == null) throw new Exception("Null String");
		String trimmedS = s.trim();

		if(trimmedS.isEmpty()) throw new Exception("Empty String");

		Pattern real = Pattern.compile("([\\+-]?)(\\d+)\\.(\\d*)");
		if(real.matcher(trimmedS).find()) throw new Exception("Numero é real, não pode ser representado pelo tipo int");

		Pattern inteiro = Pattern.compile("([\\+-]?)(\\d+)");
		Matcher matcher = inteiro.matcher(trimmedS);

		if(matcher.find()){
			String grupo2 = matcher.group(2);
			int toReturn = 0;
			for(char c: grupo2.toCharArray()){
				/*
				if(toReturn > Integer.MAX_VALUE/10)
					throw new Exception("possível ovrflow");
				*/
				int tmp = toReturn*10;
				if(tmp/10 != toReturn) throw new Exception("Overflow, numero é maior que Integer.MAX_VALUE(*)");
				int tmp2 = tmp + Character.getNumericValue(c);
				if(tmp2 < tmp) throw new Exception("Overflow, numero é maior que Integer.MAX_VALUE(+)");
				toReturn = tmp2;
			}
			if(matcher.group(1).equals("-")) toReturn *= -1;
			return toReturn;
		} else {
			throw new Exception("Formato da String não foi reconhecido(provavelmente não é número)");
		}

	}

	public static void main(String[] args) {
		System.out.printf("max value:\n%d\n", Integer.MAX_VALUE);
		/*
		for(long i = 2140000000; i < Long.MAX_VALUE; ++i){
			try{
				Integer tmp = safeStringToInteger(Long.toString(i));
				System.out.printf("%d\t|\t%d\t|%s\n", i, tmp, i==tmp?"":"Overflow");
			}catch (Exception e){
				System.out.println(e.getMessage());
			}
		}
		*/

		while (true){
			System.out.println("Digite um Número(Para sair digite 'q'):");
			String input = System.console().readLine();
			if(input.equals("q")) break;
			try {
				System.out.printf("%d\n", safeStringToInteger(input));
			} catch (Exception e){
				System.out.println(e.getMessage());
			}
		}
	}
}
